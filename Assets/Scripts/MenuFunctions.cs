using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFunctions : MonoBehaviour
{
    public GameObject menuButtons;
    public GameObject option;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void StartButton()
    {
        SceneManager.LoadScene(1);
    }

    public void OptionsButton()
    {
        menuButtons.SetActive(false);
        option.SetActive(true);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

}
