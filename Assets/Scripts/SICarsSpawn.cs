using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SICarsSpawn : MonoBehaviour
{
    public float carSpawnDelay;
    public GameObject siCarBlue;
    public GameObject siCarRed;
    public GameObject siCarGreen;
    public GameObject siBossOne;
    public GameObject siBossTwo;
    public GameObject siBossThree;
    public GameObject siBossFour;

    private int siCarsCounter;
    private int bossCarsCounter;
    private int helper;

    [SerializeField] private float[] lanesArray;

    void Start()
    {
        carSpawnDelay = 2f;
        siCarsCounter = 0;
        bossCarsCounter = 0;
        helper = 25;
    }

    void Update()
    {
        carSpawnDelay -= Time.deltaTime;
        if (carSpawnDelay <= 0)
        {
            if (siCarsCounter != 0 && siCarsCounter % helper == 0)
            {
                bossCarsCounter += 1;
                BossSpawner();
                SpawnLevel();
            }
            else
            {
                Spawner();
                SpawnLevel();
            }
        }
    }

    void Spawner()
    {
        int lane = Random.Range(0, 4);
        int carNumber = Random.Range(0, 2);

        if (lane == 0 || lane == 1)
        {
            if (carNumber == 0)
            {
                GameObject car = (GameObject)Instantiate(siCarBlue, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
            else if (carNumber == 1)
            {
                GameObject car = (GameObject)Instantiate(siCarRed, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
            else if (carNumber == 2)
            {
                GameObject car = (GameObject)Instantiate(siCarGreen, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
        } else if (lane == 2 || lane == 3)
        {
            if (carNumber == 0)
            {
                Instantiate(siCarBlue, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
            else if (carNumber == 1)
            {
                Instantiate(siCarRed, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
            else if (carNumber == 2)
            {
                Instantiate(siCarGreen, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
        }
        siCarsCounter += 1;
    }

    void BossSpawner()
    {
        int lane = Random.Range(0, 4);
        int carNumber = Random.Range(0, 4);

        if (bossCarsCounter <= 50)
        {
            switch (bossCarsCounter)
            {
                case 5:
                    ScoreSystem.score += 5;
                    helper = 20;
                    break;
                case 10:
                    ScoreSystem.score += 10;
                    helper = 15;
                    break;
                case 25:
                    ScoreSystem.score += 25;
                    helper = 10;
                    break;
                case 50:
                    ScoreSystem.score += 50;
                    helper = 5;
                    break;
            }
        }
        else
        {
            ScoreSystem.score += 500;
        }

        if (lane == 0 || lane == 1)
        {
            if (carNumber == 0)
            {
                GameObject car = (GameObject)Instantiate(siBossOne, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
            else if (carNumber == 1)
            {
                GameObject car = (GameObject)Instantiate(siBossTwo, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
            else if (carNumber == 2)
            {
                GameObject car = (GameObject)Instantiate(siBossThree, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
            else if (carNumber == 3)
            {
                GameObject car = (GameObject)Instantiate(siBossFour, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0, 0, 180)));
                car.GetComponent<SICarsMove>().directionMovement = 1;
                car.GetComponent<SICarsMove>().siCarSpeed *= 2;
            }
        }
        else if (lane == 2 || lane == 3)
        {
            if (carNumber == 0)
            {
                Instantiate(siBossOne, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
            else if (carNumber == 1)
            {
                Instantiate(siBossTwo, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
            else if (carNumber == 2)
            {
                Instantiate(siBossThree, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
            else if (carNumber == 3)
            {
                Instantiate(siBossFour, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
            }
        }
        siCarsCounter += 1;
    }

    void SpawnLevel()
    {
        if (siCarsCounter >= 1)
        {
            carSpawnDelay = 6f;
        }
        if (siCarsCounter > 5)
        {
            carSpawnDelay = 5f;
        }
        if (siCarsCounter > 15)
        {
            carSpawnDelay = 4f;
        }
        if (siCarsCounter > 20)
        {
            carSpawnDelay = 3f;
        }
        if (siCarsCounter > 30)
        {
            carSpawnDelay = 2f;
        }
    }

}
