using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SICarsMove : MonoBehaviour
{
    public float siCarSpeed = 5f;
    private float siCarPosition;
    public int directionMovement = -1;

    public float damage = 20f;

    public int carPoints;

    void Update()
    {
        this.gameObject.transform.Translate(new Vector3(0, directionMovement, 0) * siCarSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if(obj.gameObject.tag == "Player")
        {
            obj.gameObject.GetComponent<PlayerControl>().durability -= damage;
            Destroy(this.gameObject);
            ScoreSystem.score -= carPoints;
        }
        else if (obj.gameObject.tag == "EndOfTheRoad")
        {
            Destroy(this.gameObject);
            ScoreSystem.score += carPoints;
        }
    }
}
