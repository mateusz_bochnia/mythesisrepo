using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonuses : MonoBehaviour
{
    [Header("Type of bonus")]
    public bool isSmallRepair;
    public bool isMediumRepair;
    public bool isBigRepair;
    public bool isBooster;
    public bool isMegaBooster;
    public bool isSmallRetarder;
    public bool isSmallAccelerant;
    public bool isBigAccelerant;
    public bool isShield;
    public bool isLiveAddition;
    public bool isLifeSubtraction;


    [Header("Bonuses Settings")]
    public float bonusSpeed;
    public float bonusTime;
    public int bonusPoints;

    [Header("Durability&Life Settings")]
    public float repairPoints;
    public int life;

    [Header("Shield Settings")]
    public GameObject shield;
    private GameObject playerCar;
    private Vector3 carPosition;

    [Header("Speed/Booster Settings")]
    public float speedBoost;
    public float duration;
    private bool isActivated = false;

    void Update()
    {
        this.gameObject.transform.Translate(new Vector3(0, -1, 0) * bonusSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.gameObject.tag == "Player" ^ obj.gameObject.tag == "Shield")
        {
            if (isBigRepair == true)
            {
                obj.gameObject.GetComponent<PlayerControl>().durability += repairPoints;
                Destroy(this.gameObject);
                ScoreSystem.score += bonusPoints;
            }
            else if (isMediumRepair == true)
            {
                obj.gameObject.GetComponent<PlayerControl>().durability += repairPoints;
                Destroy(this.gameObject);
                ScoreSystem.score += bonusPoints;
            }
            else if (isSmallRepair == true)
            {
                obj.gameObject.GetComponent<PlayerControl>().durability += repairPoints;
                Destroy(this.gameObject);
                ScoreSystem.score += bonusPoints;
            }
            else if (isLiveAddition == true)
            {
                LifeSystem.lifes += life;
                Destroy(this.gameObject);
                ScoreSystem.score += bonusPoints;
            }
            else if (isLifeSubtraction == true)
            {
                LifeSystem.lifes -= life;
                Destroy(this.gameObject);
                ScoreSystem.score -= bonusPoints;
            }
            else if (isSmallAccelerant == true)
            {
                obj.gameObject.GetComponent<PlayerControl>().durability += repairPoints;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                isActivated = true;
                StartCoroutine("SpeedBooster");
                ScoreSystem.score += bonusPoints;
            }
            else if (isSmallRetarder == true)
            {
                obj.gameObject.GetComponent<PlayerControl>().durability -= repairPoints;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                isActivated = true;
                StartCoroutine("SpeedBooster");
                ScoreSystem.score += bonusPoints;
            }
            else if (isBooster == true)
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                isActivated = true;
                StartCoroutine("SpeedBooster");
                ScoreSystem.score += bonusPoints;
            }
            else if (isMegaBooster == true)
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                isActivated = true;
                StartCoroutine("SpeedBooster");
                ScoreSystem.score += bonusPoints;
            }
            else if (isBigAccelerant == true)
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                isActivated = true;
                StartCoroutine("SpeedBooster");
                ScoreSystem.score += bonusPoints;
            }
            else if (isShield == true)
            {
                playerCar = GameObject.FindWithTag("Player");
                obj.gameObject.tag = "Shield";
                carPosition = playerCar.transform.position;
                carPosition.z = -0.2f;
                GameObject shieldObj = (GameObject)Instantiate(shield, carPosition, Quaternion.identity);
                shieldObj.transform.parent = playerCar.transform;
                Destroy(this.gameObject);
                ScoreSystem.score += bonusPoints;
                Shield.counter += 1;
            }
        }
        else if (obj.gameObject.tag == "EndOfTheRoad")
        {
            Destroy(this.gameObject);
        }

    }

    IEnumerator SpeedBooster()
    {
        while (duration > 0)
        {
            duration -= Time.deltaTime / speedBoost;
            Time.timeScale = speedBoost;
            yield return null;
        }
        Time.timeScale = 1f;
        Destroy(this.gameObject);
    }
}
