using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSystem : MonoBehaviour
{
    public GameObject playerCarPrefab;
    public TextMesh durabilityNumber;
    static public int lifes = 3;
    private GameObject playerCar;

    void Start()
    {
        playerCar = (GameObject)Instantiate(playerCarPrefab, new Vector3(-0.09f, -3.68f, 0), Quaternion.identity);
    }

    void Update()
    {
        if (playerCar.GetComponent<PlayerControl>().durability <= 0)
        {
            Destroy(playerCar);
            lifes--;
            if(lifes > 0)
            {
                StartCoroutine("SpawningMode");
            }
        }else if (playerCar.GetComponent<PlayerControl>().durability > 100)
        {
            playerCar.GetComponent<PlayerControl>().durability = playerCar.GetComponent<PlayerControl>().maxDurability;
        }

        durabilityNumber.text = "Life: "+lifes+"/3 "+"Druability: " + playerCar.GetComponent<PlayerControl>().durability + "/" + playerCar.GetComponent<PlayerControl>().maxDurability;
    }

    IEnumerator SpawningMode()
    {
        playerCar = (GameObject)Instantiate(playerCarPrefab, new Vector3(-0.09f, -3.68f, 0), Quaternion.identity);
        playerCar.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.25f);
        playerCar.tag = "UntouchableMode";
        yield return new WaitForSeconds(2);
        playerCar.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        playerCar.tag = "Player";
    }
}
