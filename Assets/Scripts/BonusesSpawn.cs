using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusesSpawn : MonoBehaviour
{
    public GameObject[] BonussesNumber;
    public int minDelay;
    public int maxDelay;

    private float Delay;
    private int BonusCounter;

    void Start()
    {
        Delay = Random.Range(minDelay, maxDelay);
        BonusCounter = 0;
    }

    void Update()
    {
        Delay -= Time.deltaTime;
        if (Delay <= 0)
        {
            BonusSpawn();
            Delay = Random.Range(minDelay, maxDelay);
            Debug.Log("Bonus Counter: " + BonusCounter + " minDelay: " + minDelay + " maxDelay: " + maxDelay + " Delay: " + Delay);
        }
    }

    void BonusSpawn()
    {
        Instantiate(BonussesNumber[(int)Random.Range(0, 10)], new Vector3(Random.Range(-2.33f, 2.36f), 5.9f, 0), Quaternion.identity);
        BonusCounter += 1;
        BonusDelayLevel();
    }

    void BonusDelayLevel()
    {
        if (BonusCounter > 1)
        {
            minDelay = 4;
            maxDelay = 6;
        }
            if (BonusCounter >= 5)
        {
            minDelay = 15;
            maxDelay = 20;
        }
        if (BonusCounter >= 11)
        {
            minDelay = 30;
            maxDelay = 40;
        }
        if (BonusCounter >= 21)
        {
            minDelay = 60;
            maxDelay = 80;
        }
        if (BonusCounter > 50) 
        {
            minDelay = 120;
            maxDelay = 160;
        }
    }
}
