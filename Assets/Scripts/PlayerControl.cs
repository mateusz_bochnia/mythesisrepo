 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float carSpeed = 2f;
    private Vector3 carPosition;

    public float maxDurability = 100f;
    [HideInInspector]
    public float durability;

    void Start()
    {
        carPosition = this.gameObject.transform.position;
        durability = maxDurability;
    }

    void Update()
    {
        carPosition.x += Input.GetAxis("Horizontal") * carSpeed * Time.deltaTime;
        carPosition.y += Input.GetAxis("Vertical") * carSpeed * Time.deltaTime;
        carPosition.x = Mathf.Clamp(carPosition.x, -2.33f, 2.33f);
        carPosition.y = Mathf.Clamp(carPosition.y, -4.06f, 4.04f);
        this.gameObject.transform.position = carPosition;
    }
}
