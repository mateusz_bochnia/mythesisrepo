using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    public Slider volume;
    public GameObject menu;

    void Start()
    {
        volume.value = AudioListener.volume;

        volume.onValueChanged.AddListener(delegate { VolumeChange(); });
    }

    public void VolumeChange()
    {
        AudioListener.volume = volume.value;
    }

    public void OnButtonClick()
    {
        this.gameObject.SetActive(false);
        menu.SetActive(true);
    }
}
