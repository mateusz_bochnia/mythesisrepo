using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public float duration;
    static public int counter;

    void Update()
    {
        duration -= Time.deltaTime;
        if(duration <= 0)
        {
            this.gameObject.transform.parent.tag = "Player";
            Destroy(this.gameObject);

            if(counter > 5)
            {
                duration = 8;
            }
            if (counter > 10)
            {
                duration = 6;
            }
            if (counter > 15)
            {
                duration = 5;
            }
        }
    }
}
