using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadScrolling : MonoBehaviour
{
    public float scrollingSpeed;
    private Vector2 offset;

    void Update()
    {
        offset = new Vector2(0,Time.time * scrollingSpeed);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
