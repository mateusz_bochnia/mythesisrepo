using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    static public int score;
    private float delay = 1;
    public TextMesh scoreText;

    void Start()
    {
        score = 0;
    }

    void Update()
    {
        scoreText.text = "Score: " + score;
        delay -= Time.deltaTime;
        if (delay <= 0)
        {
            score += 1;
            delay = 1;
        }
        if (score < 0)
        {
            score = 0;
        }
    }
}
